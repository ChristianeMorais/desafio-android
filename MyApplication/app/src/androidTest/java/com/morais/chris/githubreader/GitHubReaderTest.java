package com.morais.chris.githubreader;

import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.ViewAssertion;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.morais.chris.githubreader.activities.MainActivity;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

/**
 * Created by chris.morais on 16/07/16
 */
@RunWith(AndroidJUnit4.class)
public class GitHubReaderTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setUp() {
        onView(isRoot()).perform(waitFor(500));
    }

    @Test
    public void exibeListaRepositorios() throws InterruptedException {
        onView(withId(R.id.rv_repositorios)).check(new RecyclerViewItemCountAssertion(30, true));
    }

    @Test
    public void abrirListaPullRequests() {
        onView(withId(R.id.rv_repositorios)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        onView(isRoot()).perform(waitFor(500));

        onView(withId(R.id.rv_pullrequests)).check(new RecyclerViewItemCountAssertion(0, false));
    }

    @Test
    public void abrirPaginaPullRequest() {
        onView(withId(R.id.rv_repositorios)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        onView(isRoot()).perform(waitFor(500));

        onView(withId(R.id.rv_pullrequests)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
    }

    /**
     * Aguarda a execução do testes por um tempo determinado
     *
     * @param millis tempo de espera em milisegundos
     * @return ViewAction
     */
    public static ViewAction waitFor(final long millis) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isRoot();
            }

            @Override
            public String getDescription() {
                return "wait for a view during " + millis + " millis.";
            }

            @Override
            public void perform(final UiController uiController, final View view) {
                uiController.loopMainThreadUntilIdle();
                final long startTime = System.currentTimeMillis();
                final long endTime = startTime + millis;

                //noinspection StatementWithEmptyBody
                while (System.currentTimeMillis() < endTime) ;
            }
        };
    }

    /**
     *  Classe para validar a quantidade de itens carregados no RecyclerView
     */
    public class RecyclerViewItemCountAssertion implements ViewAssertion {
        private final int expectedCount;
        boolean assertTrue;

        /**
         *
         * @param expectedCount Quantidade que deve ter na lista
         * @param assertTrue se a validação deve retornar true ou false
         */
        public RecyclerViewItemCountAssertion(int expectedCount, boolean assertTrue) {
            this.expectedCount = expectedCount;
            this.assertTrue = assertTrue;
        }

        @Override
        public void check(View view, NoMatchingViewException noViewFoundException) {
            if (noViewFoundException != null) {
                throw noViewFoundException;
            }

            RecyclerView recyclerView = (RecyclerView) view;
            RecyclerView.Adapter adapter = recyclerView.getAdapter();

            if (assertTrue) {
                assertThat(adapter.getItemCount(), is(expectedCount));
            } else {
                assertThat(adapter.getItemCount(), not(is(expectedCount)));
            }
        }
    }

}
