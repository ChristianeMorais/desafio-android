package com.morais.chris.githubreader;

import android.app.Application;
import android.content.Context;

/**
 * Created by chris.morais on 16/07/16
 */
public class GitApplication extends Application {
    public static Application appContext;

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }
}
