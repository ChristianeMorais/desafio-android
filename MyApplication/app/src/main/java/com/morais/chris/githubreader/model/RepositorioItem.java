package com.morais.chris.githubreader.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by chris.morais on 16/07/16
 */
public class RepositorioItem {
    @SerializedName("name")
    private String name;
    @SerializedName("owner")
    private Usuario owner;
    @SerializedName("description")
    private String description;
    @SerializedName("stargazers_count")
    private int stargazersCount;
    @SerializedName("forks_count")
    private int forksCount;

    public String getName() {
        return name;
    }

    public Usuario getOwner() {
        return owner;
    }

    public String getDescription() {
        return description;
    }

    public int getStargazersCount() {
        return stargazersCount;
    }

    public int getForksCount() {
        return forksCount;
    }
}
