package com.morais.chris.githubreader.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.morais.chris.githubreader.R;
import com.morais.chris.githubreader.model.PullRequest;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by chris.morais on 16/07/16
 */
public class PullRequestsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<PullRequest> pullRequests;

    private View.OnClickListener itemClique = null;
    private Context context;

    public PullRequestsAdapter(View.OnClickListener itemClique, List<PullRequest> pullRequests) {
        this.itemClique = itemClique;
        this.pullRequests = pullRequests;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();

        View view = LayoutInflater.from(context).inflate(R.layout.item_pull_request, parent, false);

        view.setOnClickListener(itemClique);

        return new PullRequestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        PullRequestViewHolder viewHolder = (PullRequestViewHolder) holder;

        PullRequest item = pullRequests.get(position);

        viewHolder.tvNomeUsuario.setText(item.getUser().getLogin());
        viewHolder.tvNomePullRequest.setText(item.getTitle());
        viewHolder.tvDescricaoPullRequest.setText(item.getBody());

        viewHolder.tvDataCriacao.setText(formatarData(item.getCreatedAt()));

        Glide.with(context)
                .load(item.getUser().getAvatarUrl())
                .error(R.drawable.default_photo)
                .placeholder(R.drawable.default_photo)
                .into(viewHolder.ivFoto);

    }

    private String formatarData(String createdAt) {

        String retorno = "Criado em: %s às %s";

        String data = createdAt.substring(0, createdAt.indexOf("T"));
        String hora = createdAt.substring(createdAt.indexOf("T") + 1, createdAt.length() - 1);

        return String.format(retorno, data, hora);
    }

    @Override
    public int getItemCount() {
        return pullRequests.size();
    }

    static class PullRequestViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_foto)
        ImageView ivFoto;

        @BindView(R.id.tv_nome_usuario)
        TextView tvNomeUsuario;

        @BindView(R.id.tv_nome_pullrequest)
        TextView tvNomePullRequest;

        @BindView(R.id.tv_descricao_pullrequest)
        TextView tvDescricaoPullRequest;

        @BindView(R.id.tv_data_criacao)
        TextView tvDataCriacao;

        public PullRequestViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
