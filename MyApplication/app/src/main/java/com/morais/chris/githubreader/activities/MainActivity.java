package com.morais.chris.githubreader.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.android.volley.VolleyError;
import com.morais.chris.githubreader.GitApplication;
import com.morais.chris.githubreader.R;
import com.morais.chris.githubreader.adapters.RepositoriosAdapter;
import com.morais.chris.githubreader.interfaces.IRepositoriosResponse;
import com.morais.chris.githubreader.model.Repositorio;
import com.morais.chris.githubreader.model.RepositorioItem;
import com.morais.chris.githubreader.utils.EndlessRecyclerViewScrollListener;
import com.morais.chris.githubreader.utils.Util;
import com.morais.chris.githubreader.volley.VolleyRequest;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements IRepositoriosResponse {

    private int pagina = 1;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_repositorios)
    RecyclerView recyclerView;

    private static final String REPOSITORIO_SAVE_STATE = "REPOSITORIO_SAVE_STATE";

    private RepositoriosAdapter adapter;
    private List<RepositorioItem> repositorioItems;

    private Parcelable parcelable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        GitApplication.appContext = getApplication();

        if (savedInstanceState == null) {
            iniciarToolbar();
            iniciarRecyclerView();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        parcelable = recyclerView.getLayoutManager().onSaveInstanceState();

        outState.putParcelable(REPOSITORIO_SAVE_STATE, parcelable);

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);

        if (state != null)
            parcelable = state.getParcelable(REPOSITORIO_SAVE_STATE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (parcelable != null) {
            recyclerView.getLayoutManager().onRestoreInstanceState(parcelable);
        } else {
            Util.exibirDialog(this, getString(R.string.carregando), getString(R.string.aguarde_carregando));
            carregarLista();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if(adapter != null) {
            recyclerView.setAdapter(adapter);
        }
    }

    private void iniciarToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.app_name);
        }
    }

    private void iniciarRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                pagina = page;
                carregarLista();
            }
        });
    }

    private void carregarLista() {
        VolleyRequest.obterRepositorios(this, pagina);
    }

    @Override
    public void obterRepositoriosSuccess(Repositorio response) {
        if (pagina == 1) {
            repositorioItems = response.getItems();

            adapter = new RepositoriosAdapter(new ItemClique(recyclerView.getLayoutManager()), repositorioItems);
            recyclerView.setAdapter(adapter);

            Util.fecharDialog();
        } else {
            repositorioItems.addAll(response.getItems());
            adapter.notifyItemRangeInserted(adapter.getItemCount(), repositorioItems.size() - 1);
        }
    }

    @Override
    public void obterRepositoriosError(VolleyError volleyError) {
        Util.fecharDialog();
        Util.exibirSnackBar(getCurrentFocus(), getString(R.string.erro_generico));
    }

    /**
     * Classe para tratar o evento de clique no item do recyclerView
     */
    class ItemClique implements View.OnClickListener {
        private RecyclerView.LayoutManager layoutManager;

        public ItemClique(RecyclerView.LayoutManager layoutManager) {
            this.layoutManager = layoutManager;
        }

        @Override
        public void onClick(View v) {
            int pos = layoutManager.getPosition(v);

            Intent intent = new Intent(getBaseContext(), PullRequestActivity.class);
            intent.putExtra(Util.REPOSITORIO_SELECIONADO, repositorioItems.get(pos).getName());
            intent.putExtra(Util.CRIADOR_REPOSITORIO_SELECIONADO, repositorioItems.get(pos).getOwner().getLogin());

            startActivity(intent);
        }
    }
}
