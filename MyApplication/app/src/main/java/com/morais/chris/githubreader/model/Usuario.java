package com.morais.chris.githubreader.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by chris.morais on 16/07/16
 */
public class Usuario {
    @SerializedName("login")
    private String login;
    @SerializedName("avatar_url")
    private String avatarUrl;

    public String getLogin() {
        return login;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }
}
