package com.morais.chris.githubreader.interfaces;

import com.android.volley.VolleyError;
import com.morais.chris.githubreader.model.PullRequest;

import java.util.List;

/**
 * Created by chris.morais on 16/07/16
 */
public interface IPullResquestResponse {
    void obterPullRequestsSuccess(List<PullRequest> pullRequests);

    void obterPullRequestsError(VolleyError volleyError);
}
