package com.morais.chris.githubreader.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by chris.morais on 16/07/16
 */
public class PullRequest {
    @SerializedName("html_url")
    private String htmlUrl;
    @SerializedName("title")
    private String title;
    @SerializedName("user")
    private Usuario user;
    @SerializedName("body")
    private String body;
    @SerializedName("created_at")
    private String createdAt;

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public String getTitle() {
        return title;
    }

    public Usuario getUser() {
        return user;
    }

    public String getBody() {
        return body;
    }

    public String getCreatedAt() {
        return createdAt;
    }
}
