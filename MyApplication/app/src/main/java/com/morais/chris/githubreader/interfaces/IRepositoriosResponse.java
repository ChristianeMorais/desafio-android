package com.morais.chris.githubreader.interfaces;

import com.android.volley.VolleyError;
import com.morais.chris.githubreader.model.Repositorio;

/**
 * Created by chris.morais on 16/07/16
 */
public interface IRepositoriosResponse {
    void obterRepositoriosSuccess(Repositorio response);

    void obterRepositoriosError(VolleyError volleyError);
}
