package com.morais.chris.githubreader.volley;

import android.net.Uri;
import android.util.Log;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.morais.chris.githubreader.GitApplication;
import com.morais.chris.githubreader.interfaces.IPullResquestResponse;
import com.morais.chris.githubreader.interfaces.IRepositoriosResponse;
import com.morais.chris.githubreader.model.PullRequest;
import com.morais.chris.githubreader.model.Repositorio;

import java.util.Arrays;

/**
 * Created by chris.morais on 16/07/16
 */
public class VolleyRequest {
    static RequestQueue mRequestQueue;

    /**
     * Inicia a fila de requisições
     */
    private static void iniciarQueue() {
        Cache cache = new DiskBasedCache(GitApplication.appContext.getCacheDir(), 1024 * 1024); // 1MB cap
        Network network = new BasicNetwork(new HurlStack());

        mRequestQueue = new RequestQueue(cache, network);
        mRequestQueue.start();
    }

    public static void obterRepositorios(final IRepositoriosResponse repositoriosResponse, int pagina) {
        if (mRequestQueue == null) iniciarQueue();

        Uri.Builder builder = Uri.parse("https://api.github.com/search/repositories").buildUpon();
        builder.appendQueryParameter("q", "language:Java")
                .appendQueryParameter("sort", "stars")
                .appendQueryParameter("page", String.valueOf(pagina));

        GsonRequest<Repositorio> request = new GsonRequest<>(Request.Method.GET, builder.toString(), Repositorio.class, null,
                new Response.Listener<Repositorio>() {
                    @Override
                    public void onResponse(Repositorio response) {
                        repositoriosResponse.obterRepositoriosSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.e("Repositorios", "onErrorResponse: ", volleyError);
                        repositoriosResponse.obterRepositoriosError(volleyError);
                    }
                }
        );

        request.setTag("Repositorios");

        mRequestQueue.add(request);
    }

    public static void obterPullRequest(final IPullResquestResponse pullResquestResponse, String criador, final String repositorio) {

        if (criador == null || repositorio == null) return;

        if (mRequestQueue == null) iniciarQueue();

        String url = String.format("https://api.github.com/repos/%s/%s/pulls", criador, repositorio);

        GsonRequest<PullRequest[]> request = new GsonRequest<>(Request.Method.GET, url, PullRequest[].class, null,
                new Response.Listener<PullRequest[]>() {
                    @Override
                    public void onResponse(PullRequest[] response) {
                        pullResquestResponse.obterPullRequestsSuccess(Arrays.asList(response));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.e("PullRequest", "onErrorResponse: ", volleyError);
                        pullResquestResponse.obterPullRequestsError(volleyError);
                    }
                }
        );

        request.setTag("PullRequest");

        mRequestQueue.add(request);
    }
}
