package com.morais.chris.githubreader.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.morais.chris.githubreader.R;
import com.morais.chris.githubreader.model.RepositorioItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by chris.morais on 16/07/16
 */
public class RepositoriosAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<RepositorioItem> repositorioItems;

    private View.OnClickListener itemClique = null;
    private Context context;

    public RepositoriosAdapter(View.OnClickListener itemClique, List<RepositorioItem> repositorioItems) {
        this.itemClique = itemClique;
        this.repositorioItems = repositorioItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();

        View view = LayoutInflater.from(context).inflate(R.layout.item_recycler_view, parent, false);

        view.setOnClickListener(itemClique);

        return new RepositorioViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        RepositorioViewHolder viewHolder = (RepositorioViewHolder) holder;

        RepositorioItem item = repositorioItems.get(position);

        viewHolder.tvNomeUsuario.setText(item.getOwner().getLogin());
        viewHolder.tvNomeRepositorio.setText(item.getName());
        viewHolder.tvDescricaoRepositorio.setText(item.getDescription());
        viewHolder.tvQtdeEstrelas.setText(String.valueOf(item.getStargazersCount()));
        viewHolder.tvQtdeForks.setText(String.valueOf(item.getForksCount()));

        Glide.with(context)
                .load(item.getOwner().getAvatarUrl())
                .error(R.drawable.default_photo)
                .placeholder(R.drawable.default_photo)
                .into(viewHolder.ivFoto);
    }

    @Override
    public int getItemCount() {
        return repositorioItems.size();
    }

    static class RepositorioViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_foto)
        ImageView ivFoto;

        @BindView(R.id.tv_nome_usuario)
        TextView tvNomeUsuario;

        @BindView(R.id.tv_nome_repositorio)
        TextView tvNomeRepositorio;

        @BindView(R.id.tv_descricao_repositorio)
        TextView tvDescricaoRepositorio;

        @BindView(R.id.tv_qtde_estrelas)
        TextView tvQtdeEstrelas;

        @BindView(R.id.tv_qtde_forks)
        TextView tvQtdeForks;

        public RepositorioViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
