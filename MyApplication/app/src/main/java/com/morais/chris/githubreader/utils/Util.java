package com.morais.chris.githubreader.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * Created by chris.morais on 16/07/16
 */
public class Util {

    public static final String REPOSITORIO_SELECIONADO  = "REPOSITORIO_SELECIONADO";
    public static final String CRIADOR_REPOSITORIO_SELECIONADO  = "CRIADOR_REPOSITORIO_SELECIONADO";

    private static ProgressDialog progressDialog;

    public static void exibirSnackBar(View view, String mensagem) {
        if (view == null || mensagem.isEmpty())
            return;

        Snackbar.make(view, mensagem, Snackbar.LENGTH_SHORT).show();
    }

    public static void exibirDialog(Context context, String titulo, String mensagem) {
        progressDialog = ProgressDialog.show(context, titulo, mensagem, false, true);
    }

    public static void fecharDialog() {
        progressDialog.dismiss();
    }
}

