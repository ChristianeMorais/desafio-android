package com.morais.chris.githubreader.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chris.morais on 16/07/16
 */
public class Repositorio {

    @SerializedName("items")
    private List<RepositorioItem> items = new ArrayList<>();

    public List<RepositorioItem> getItems() {
        return items;
    }
}