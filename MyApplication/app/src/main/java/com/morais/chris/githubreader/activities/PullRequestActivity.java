package com.morais.chris.githubreader.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.VolleyError;
import com.morais.chris.githubreader.R;
import com.morais.chris.githubreader.adapters.PullRequestsAdapter;
import com.morais.chris.githubreader.interfaces.IPullResquestResponse;
import com.morais.chris.githubreader.model.PullRequest;
import com.morais.chris.githubreader.utils.Util;
import com.morais.chris.githubreader.volley.VolleyRequest;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PullRequestActivity extends AppCompatActivity implements IPullResquestResponse {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_pullrequests)
    RecyclerView recyclerView;

    private static final String PULLREQUESTS_SAVE_STATE = "PULLREQUESTS_SAVE_STATE";

    private List<PullRequest> pullRequests;
    private PullRequestsAdapter adapter;
    private Parcelable parcelable;

    private String criador;
    private String repositorio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);
        ButterKnife.bind(this);

        if (savedInstanceState == null) {
            criador = getIntent().getStringExtra(Util.CRIADOR_REPOSITORIO_SELECIONADO);
            repositorio = getIntent().getStringExtra(Util.REPOSITORIO_SELECIONADO);

            if (criador.isEmpty() || repositorio.isEmpty()) {
                finish();
            }

            iniciarToolbar();
            iniciarRecyclerView();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        parcelable = recyclerView.getLayoutManager().onSaveInstanceState();
        outState.putParcelable(PULLREQUESTS_SAVE_STATE, parcelable);

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState != null) {
            parcelable = savedInstanceState.getParcelable(PULLREQUESTS_SAVE_STATE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (parcelable != null) {
            recyclerView.getLayoutManager().onRestoreInstanceState(parcelable);
        } else {
            Util.exibirDialog(this, getString(R.string.carregando), getString(R.string.aguarde_carregando));
            VolleyRequest.obterPullRequest(this, criador, repositorio);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if(adapter != null) {
            recyclerView.setAdapter(adapter);
        }
    }

    private void iniciarToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(repositorio.toUpperCase());
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void iniciarRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void obterPullRequestsSuccess(List<PullRequest> pullRequests) {
        this.pullRequests = pullRequests;

        adapter = new PullRequestsAdapter(new ItemClique(recyclerView.getLayoutManager()), pullRequests);
        recyclerView.setAdapter(adapter);

        Util.fecharDialog();
    }

    @Override
    public void obterPullRequestsError(VolleyError volleyError) {
        Util.fecharDialog();
        finish();
    }

    /**
     * Classe para tratar o evento de clique no item do recyclerView
     */
    class ItemClique implements View.OnClickListener {
        private RecyclerView.LayoutManager layoutManager;

        public ItemClique(RecyclerView.LayoutManager layoutManager) {
            this.layoutManager = layoutManager;
        }

        @Override
        public void onClick(View v) {
            int pos = layoutManager.getPosition(v);

            Uri webpage = Uri.parse(pullRequests.get(pos).getHtmlUrl());
            Intent intent = new Intent(Intent.ACTION_VIEW, webpage);

            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
    }
}
